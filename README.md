# MySquare APP

This is a sample application written in Kotlin using MVVM architecture.

### Why MVVM ?

There are no particular advantages in this case using MVVM with Databinding but it allows you to avoid NullPointerException in a safe way, because setting/getting values ​​to/from views is completely managed by the databinding system and you do not have to care about the views if they are created or not.  

### Dependencies

* OkHttp
* Retrofit
* Koin
* Glide
* Gson
* RxJava
* Android Databinding