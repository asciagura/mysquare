package com.andreasciagura.mysquare

import androidx.test.InstrumentationRegistry
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {

    @Rule
    val mainActivityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testSearchFieldIsEmptyOnStartup_isTrue() {
        onView(withId(R.id.search_edit_text)).check(matches(withText("")))
    }

    @Test
    fun testProgressRingIsVisibleOnSearch_isTrue() {
        onView(withId(R.id.search_edit_text)).perform(typeText("Westminster London"))
        onView(withId(R.id.search_button)).perform(click())
        onView(withId(R.id.progress_circular)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

    fun testProgressRingIsNotVisibleOnStartup_isTrue() {
        onView(withId(R.id.progress_circular)).check(matches(withEffectiveVisibility(Visibility.GONE)))
    }


}
