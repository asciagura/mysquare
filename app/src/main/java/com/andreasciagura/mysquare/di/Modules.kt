package com.andreasciagura.mysquare.di

import com.andreasciagura.mysquare.api.FoursquareAPI
import com.andreasciagura.mysquare.datasource.VenueDataSource
import com.andreasciagura.mysquare.viewmodel.VenueSearchViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val appModule = module {

    single {
        OkHttpClient.Builder()
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(20, TimeUnit.SECONDS)
            .readTimeout(20, TimeUnit.SECONDS)
            .addInterceptor(
                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            )
            .build()
    }

    single {
        Retrofit.Builder()
            .baseUrl("https://api.foursquare.com/")
            .client(get())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    }

    //Single instance of api interface
    single {
        get<Retrofit>().create(FoursquareAPI::class.java)
    }

    single { VenueDataSource(get()) }

    viewModel { VenueSearchViewModel(get()) }
}
