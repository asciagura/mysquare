package com.andreasciagura.mysquare.api

import com.andreasciagura.mysquare.MySquareApplication
import com.andreasciagura.mysquare.models.APIResponse
import com.andreasciagura.mysquare.models.Venue
import com.andreasciagura.mysquare.models.VenueResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FoursquareAPI {
    @GET("/v2/venues/search?client_id=${MySquareApplication.CLIENT_ID}&client_secret=${MySquareApplication.CLIENT_SECRET}&v=20190228")
    fun searchVenues(@Query("near") query: String): Single<APIResponse<VenueResponse>>
}