package com.andreasciagura.mysquare

import android.app.Application
import com.andreasciagura.mysquare.di.appModule
import org.koin.android.ext.android.startKoin

class MySquareApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule))
    }

    companion object {
        internal const val CLIENT_ID = "OTL052AB2SHNXYAO5YDXAVBHR2SUROTGC01GCDMD3YWM3S1M"
        internal const val CLIENT_SECRET = "PIA1PMCOVZPIN2ZMNQ2WIPUYLYGAFNBBA1QEEE5UDVPBI1XF"
    }
}