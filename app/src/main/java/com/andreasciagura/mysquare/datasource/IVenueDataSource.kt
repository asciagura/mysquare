package com.andreasciagura.mysquare.datasource

import androidx.lifecycle.LiveData
import com.andreasciagura.mysquare.models.Venue
import io.reactivex.disposables.Disposable

interface IVenueDataSource : Disposable {
    fun searchVenues(queryText: String?, onError: (Throwable) -> Unit): LiveData<List<Venue>>
}