package com.andreasciagura.mysquare.datasource

import com.andreasciagura.mysquare.api.FoursquareAPI
import com.andreasciagura.mysquare.models.Venue
import io.reactivex.disposables.CompositeDisposable

class VenueDataSource(private val foursquareAPI: FoursquareAPI) : IVenueDataSource {

    /**
     * Check if datasource have some pending request
     */
    override fun isDisposed(): Boolean {
        return disposeBag.isDisposed
    }

    /**
     * Dispose all pending requests
     */
    override fun dispose() {
        disposeBag.dispose()
    }

    private val disposeBag = CompositeDisposable()

    override fun searchVenues(queryText: String?, onError: (Throwable) -> Unit): LiveData<List<Venue>> {
        val result = MutableLiveData<List<Venue>>()
        if (queryText.isNullOrBlank())
            throw IllegalArgumentException(EMPTY_SEARCH_FIELD)
        else {
            foursquareAPI.searchVenues(queryText)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    response?.let {
                        if (it.meta.isSuccessful())
                            result.postValue(it.response.venues)
                    }
                }, { ex ->
                    result.postValue(emptyList())
                    onError(ex)
                }).apply { disposeBag.add(this) }
        }
        return result
    }

    companion object {
        //Used const to avoid string resource loading
        const val EMPTY_SEARCH_FIELD = "Search field can't be empty"
    }
}