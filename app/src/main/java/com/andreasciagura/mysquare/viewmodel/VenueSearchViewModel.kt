package com.andreasciagura.mysquare.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.andreasciagura.mysquare.datasource.IVenueDataSource
import com.andreasciagura.mysquare.datasource.VenueDataSource
import com.andreasciagura.mysquare.models.Venue

class VenueSearchViewModel(private val venueDataSource: VenueDataSource) : ViewModel() {


    var queryText: String = ""
    var cachedResult = MutableLiveData<List<Venue>>()
    private val networkException = MutableLiveData<String>()

    fun getNetworkError(): LiveData<String> = networkException

    fun searchNearBy(): LiveData<List<Venue>> {
        return venueDataSource.searchVenues(queryText) {
            networkException.postValue(it.message ?: "Unknown exception")
        }
    }
}