package com.andreasciagura.mysquare.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andreasciagura.mysquare.databinding.VenueLayoutItemBinding
import com.andreasciagura.mysquare.models.Venue
import com.bumptech.glide.Glide
import io.reactivex.disposables.Disposable

class VenueAdapter : RecyclerView.Adapter<VenueAdapter.VenueHolder>() {
    var items: List<Venue> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueHolder {
        return VenueHolder(
            VenueLayoutItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: VenueHolder, position: Int) {
        holder.bindWith(items[position])
    }

    class VenueHolder(private val binding: VenueLayoutItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        internal fun bindWith(venue: Venue) {
            binding.viewModel = VenueViewModel(venue).also {
                Glide.with(binding.root).load(it.getVenueIcon()).override(88).into(binding.iconView)


            }
        }
    }
}