package com.andreasciagura.mysquare.adapters

import com.andreasciagura.mysquare.models.Venue

class VenueViewModel(private val venue: Venue) {

    fun getVenueIcon(): String = venue.getPrimaryCategory()?.getFullIconPath88() ?: ""

    fun getVenueName(): String = venue.name

    fun getVenueAddress(): String = venue.location.formattedAddress.joinToString(" ")
}