package com.andreasciagura.mysquare

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import com.andreasciagura.mysquare.adapters.VenueAdapter
import com.andreasciagura.mysquare.databinding.ActivityMainBinding
import com.andreasciagura.mysquare.viewmodel.VenueSearchViewModel

class MainActivity : AppCompatActivity() {

    private val searchViewModel by viewModel<VenueSearchViewModel>()

    private lateinit var binding: ActivityMainBinding
    private val adapter = VenueAdapter()

    private val ringVisibility = ObservableInt(View.GONE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        searchViewModel.cachedResult.observe(this@MainActivity,
            Observer {
                adapter.items = it
                ringVisibility.set(View.GONE)
            })
        searchViewModel.getNetworkError().observe(this, Observer {
            displayError(it)
            ringVisibility.set(View.GONE)
        })
        binding.apply {
            viewModel = searchViewModel
            progressVisibility = ringVisibility
            recyclerview.adapter = adapter

            //Handled enter key event to perform search
            searchEditText.setOnKeyListener { v, keyCode, event ->
                return@setOnKeyListener if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    searchButton.performClick()
                    true
                } else
                    false
            }

            searchButton.setOnClickListener(this@MainActivity::onSearchClicked)
        }
    }

    override fun onResume() {
        super.onResume()
        searchViewModel.cachedResult.value?.let { adapter.items = it }
    }

    private fun onSearchClicked(view: View) {
        ringVisibility.set(View.VISIBLE)
        searchViewModel.searchNearBy().observe(
            this@MainActivity,
            Observer { list -> searchViewModel.cachedResult.postValue(list) })
    }

    private fun displayError(message: String?) {
        message?.let {
            Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG)
                .show()
        }
    }
}
