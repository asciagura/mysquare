package com.andreasciagura.mysquare.models

data class MetaResponse(val code: Int, val requestId: String, val errorDetail: String) {
    fun isSuccessful(): Boolean = code == 200
}