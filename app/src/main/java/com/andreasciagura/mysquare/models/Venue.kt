package com.andreasciagura.mysquare.models

data class Venue(
    val id: String,
    val name: String,
    val location: Location,
    val categories: List<Category>
) {
    fun getPrimaryCategory(): Category? = categories.firstOrNull { it.primary } ?: categories.firstOrNull()
}