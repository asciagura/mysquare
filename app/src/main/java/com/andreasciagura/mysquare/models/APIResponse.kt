package com.andreasciagura.mysquare.models

data class APIResponse<T>(val response: T, val meta: MetaResponse)