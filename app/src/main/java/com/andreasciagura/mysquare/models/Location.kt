package com.andreasciagura.mysquare.models

data class Location(
    val address: String? = null,
    val formattedAddress: List<String>
) {
    fun getShowableAddress(): String = formattedAddress.joinToString(" ")
}