package com.andreasciagura.mysquare.models

data class VenueResponse(val venues: List<Venue>) {
}