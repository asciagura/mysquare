package com.andreasciagura.mysquare.models

data class Icon(
    val prefix: String,
    val suffix: String
)