package com.andreasciagura.mysquare.models

data class Category(
    val icon: Icon,
    val id: String,
    val name: String,
    val pluralName: String,
    val primary: Boolean,
    val shortName: String
) {

    fun getFullIconPath88(): String = "${icon.prefix}bg_88${icon.suffix}"
}