package com.andreasciagura.mysquare

import com.andreasciagura.mysquare.datasource.VenueDataSource
import com.andreasciagura.mysquare.di.appModule
import com.andreasciagura.mysquare.viewmodel.VenueSearchViewModel
import org.junit.After
import org.junit.Assert
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class VenueSearchViewModelTest : KoinTest {

    private val searchViewModel: VenueSearchViewModel by inject()

    @Before
    fun before() {
        startKoin(listOf(appModule))
    }

    @After
    fun after() {
        stopKoin()
    }

    @Test(expected = IllegalArgumentException::class)
    fun testEmptySearch_throwsException() {
        searchViewModel.queryText = ""
        searchViewModel.searchNearBy()
    }
}
